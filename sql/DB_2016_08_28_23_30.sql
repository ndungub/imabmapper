CREATE DATABASE  IF NOT EXISTS `imabmapper` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `imabmapper`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: localhost    Database: imabmapper
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth`
--

DROP TABLE IF EXISTS `auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth` (
  `auth_username` varchar(50) NOT NULL,
  `auth_pwd` varchar(36) NOT NULL,
  `auth_role` varchar(36) NOT NULL,
  `enabled` enum('TRUE','FALSE') DEFAULT 'TRUE',
  PRIMARY KEY (`auth_username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth`
--

LOCK TABLES `auth` WRITE;
/*!40000 ALTER TABLE `auth` DISABLE KEYS */;
INSERT INTO `auth` VALUES ('boniface','boniface','IMAB','TRUE'),('imabmapper','imabmapper','KISIMA','TRUE');
/*!40000 ALTER TABLE `auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clients` (
  `client_id` int(11) NOT NULL,
  `clientidentitytype` enum('NationalID','PASSPORT') NOT NULL DEFAULT 'NationalID',
  `clientidentity` varchar(50) NOT NULL,
  `clientphysicaladdress` varchar(50) NOT NULL,
  `clientpinno` varchar(50) NOT NULL,
  `clientpemail` varchar(50) NOT NULL,
  `clienttelno` varchar(50) NOT NULL,
  `clientcountry` varchar(50) NOT NULL,
  `clientcounty` varchar(50) NOT NULL,
  `clientsubcounty` varchar(50) NOT NULL,
  `auth_username` varchar(50) NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `FK_Client_Auth_username_idx` (`auth_username`),
  CONSTRAINT `FK_Client_Auth_username` FOREIGN KEY (`auth_username`) REFERENCES `auth` (`auth_username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projectlist`
--

DROP TABLE IF EXISTS `projectlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projectlist` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) NOT NULL,
  `creation_date` datetime NOT NULL,
  `shape_file` tinyint(4) NOT NULL DEFAULT '0',
  `project_narration` text NOT NULL,
  `auth_username` varchar(40) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `FK_Proj_Username_idx` (`auth_username`),
  CONSTRAINT `FK_Proj_Username` FOREIGN KEY (`auth_username`) REFERENCES `auth` (`auth_username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projectlist`
--

LOCK TABLES `projectlist` WRITE;
/*!40000 ALTER TABLE `projectlist` DISABLE KEYS */;
INSERT INTO `projectlist` VALUES (1,'KISIMA','2016-08-28 00:00:00',0,'defwdwewwewewew','boniface'),(2,'Donyo Sabuk','2016-08-28 21:45:42',1,'Donyo Sabuk ','boniface'),(3,'Kisima2','2016-08-28 21:50:52',1,'dsadsdsd','boniface'),(4,'Kisima3','2016-08-28 21:52:17',1,'Kisima3','boniface');
/*!40000 ALTER TABLE `projectlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchaseorder`
--

DROP TABLE IF EXISTS `purchaseorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchaseorder` (
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `clientnextofkin` varchar(100) DEFAULT NULL,
  `poi` geometry NOT NULL,
  `clientlrno` varchar(100) NOT NULL,
  `clientplotno` varchar(100) NOT NULL,
  `clientacreage` double NOT NULL DEFAULT '0',
  `clientpp` double NOT NULL DEFAULT '0',
  `clientdepositpayable` double NOT NULL DEFAULT '0',
  `clientpercentagepaid` double NOT NULL DEFAULT '0',
  `clientbalanceofpp` double NOT NULL DEFAULT '0',
  `clientrecommendedbyname` varchar(100) DEFAULT NULL,
  `clientrecommendedbytel` varchar(45) DEFAULT NULL,
  `purchasestatus` enum('PAID','ORDERED','REJECTED','DEPOSIT') NOT NULL DEFAULT 'ORDERED',
  `auth_username` varchar(50) NOT NULL,
  PRIMARY KEY (`client_id`),
  KEY `FK_Purchase_Auth_username_idx` (`auth_username`),
  KEY `FK_Purchase_project_id_idx` (`project_id`),
  CONSTRAINT `FK_Purchase_project_id` FOREIGN KEY (`project_id`) REFERENCES `projectlist` (`project_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Purchase_Auth_username` FOREIGN KEY (`auth_username`) REFERENCES `auth` (`auth_username`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Purchase_Client_id` FOREIGN KEY (`client_id`) REFERENCES `clients` (`client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchaseorder`
--

LOCK TABLES `purchaseorder` WRITE;
/*!40000 ALTER TABLE `purchaseorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `purchaseorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'imabmapper'
--

--
-- Dumping routines for database 'imabmapper'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-28 23:29:37
