<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/bootstrap/dist/css/bootstrap.min.css" />" >
	
	<!-- Font Awesome -->
	 <link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/font-awesome/css/font-awesome.min.css" />" >
	
	<!-- Jasny Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/jasny-bootstrap/dist/css/jasny-bootstrap.css" />" >
		
	<!-- Bootstrap Select -->
 	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/bootstrap-select/dist/css/bootstrap-select.min.css" />" >

    <!-- Leaflet -->
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/leaflet/dist/leaflet.css" />" >

	<!-- jqx css -->
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/jqwidgets-framework/jqwidgets/styles/jqx.base.css"/> ">
	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/jqwidgets-framework/jqwidgets/styles/jqx.energyblue.css"/> ">

	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/styles.css" />" >


	<!-- Awesome checkbox -->
	  <link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" />" >
	

    <title>Kisima</title>
    
    <style type="text/css">
	    .fileupload{
		    position:absolute;
		    z-index:2;
		    top:0;
		    left:0;
		    filter: alpha(opacity=0);
		    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		    opacity:0;
		    background-color:transparent;
		    color:transparent;
	    }
	  
    </style>
    
     <script type="text/javascript">
	     var model=[];
	     model.username="${pageContext.request.userPrincipal.name}";
     </script>
     
</head>
<body>
  
  
<div class="container-fullwidth">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">

          <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav ">
		            <li class="active"><a href="#whome" id="linkhome" onclick="root.homeView()">Home</a></li>
		            <li><a href="#projects" id="linkprojects" onclick="root.projectView()">Manage Projects</a></li>
		            <li><a href="#clients" id="linkclients" onclick="root.clientView()">Manage Clients</a></li>
	           </ul>
	           <ul class="nav navbar-nav">
	           		<li style="margin-top: 10px;">
						<div class="form-group col-md-offset-3 col-md-2">
						    <div class="dropdown">
							    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Map View</button>
						    </div>
					    </div>
	           		</li>
	           		<li style="margin-top: 10px;">
						<div class="form-group col-md-2">
						    <div class="dropdown">
							    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Table View
							    <span class="caret"></span></button>
							    <ul class="dropdown-menu">
									<!-- <li><a href="#">HTML</a></li> -->
							    </ul>
						    </div>
					    </div>
	           		</li>
	           		<li style="margin-top: 10px;">
						<div class="form-group col-md-offset-11 col-md-2">
						    <div class="dropdown">
							    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Purchase Order
							    <span class="caret"></span></button>
							    <ul class="dropdown-menu">
									<!-- <li><a href="#">HTML</a></li> -->
							    </ul>
						    </div>
					    </div>
	           		</li>
	           </ul>
	          
	           <ul class="nav navbar-nav navbar-right">
		            <li class="dropdown">
		              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		              <c:if test="${pageContext.request.userPrincipal.name != null}">${pageContext.request.userPrincipal.name}
					  </c:if> 
		  
		  			 <span class="caret"></span></a>
		              <ul class="dropdown-menu">
		                <li><a href="<c:url value="/logout" />" >Logout</a></li>
		              </ul>
		            </li>
	          </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

</div>
<div class="container-fullwidth" id="container">

</div>
    
    <script src="<c:url value="/resources/js/vendor/jquery/dist/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/js/vendor/bootstrap/dist/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/vendor/moment/min/moment.min.js" />"></script>
    
    <!-- jqx -->
	<script type="text/javascript" src="<c:url value="/resources/js/vendor/jqwidgets-framework/jqwidgets/globalization/globalize.js"/>"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/vendor/jqwidgets-framework/jqwidgets/jqx-all.js"/>"></script>

    <!-- Google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAD3A0dXN-v-mQO6JUynrscBoJa-sMnNFk"
        async defer></script>
    <!-- JQuery validation -->
    <script src="<c:url value="/resources/js/vendor/jquery-validation/dist/jquery.validate.min.js" />"></script>

	<!-- bootbox message popup -->
	<script src="<c:url value="/resources/js/vendor/bootbox/bootbox.js" />"></script>

    <!-- Leaflet -->
	<script src="<c:url value="/resources/js/vendor/leaflet/dist/leaflet.js" />"></script>
	<script src="<c:url value="/resources/js/vendor/leaflet-plugins/layer/tile/Google.js" />"></script>

	<!-- bootstrap select -->
	<script src="<c:url value="/resources/js/vendor/bootstrap-select/dist/js/bootstrap-select.min.js" />"></script>
	
	<!-- Jasny Bootstrap-->
	<script src="<c:url value="/resources/js/vendor/jasny-bootstrap/dist/js/jasny-bootstrap.js" />"></script>

	<!-- underscore -->
	<script src="<c:url value="/resources/js/vendor/underscore/underscore-min.js" />"></script>
	
		
    <!-- User script -->
   	<script src="<c:url value="/resources/js/imabmapper.js" />"></script>
</body>
</html>