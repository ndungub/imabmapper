<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <link href="<c:url value="/resources/js/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery/dist/jquery.min.js" />"></script>
    <script src="<c:url value="/resources/js/bootstrap/dist/js/bootstrap.min.js" />"></script>
    <script src="<c:url value="/resources/js/subscription.js" />"></script>

    
    <title>Kisima</title>
    
    <style type="text/css">
	    .fileupload{
		    position:absolute;
		    z-index:2;
		    top:0;
		    left:0;
		    filter: alpha(opacity=0);
		    -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
		    opacity:0;
		    background-color:transparent;
		    color:transparent;
	    }
	  
    </style>
    
     <script type="text/javascript">
	     var model=[];
	     model.username="${pageContext.request.userPrincipal.name}";
     </script>
     
</head>
<body>
  
  
<div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">

          <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#viewsubscription" id="linkviewsubscription">View Subscription</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <c:if test="${pageContext.request.userPrincipal.name != null}">${pageContext.request.userPrincipal.name}
			  </c:if> 
  
  			 <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<c:url value="/logout" />" >Logout</a></li>
              </ul>
            </li>
          </ul>

          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

   <div class="row">
		<div class="panel panel-default" id="pnviewsubscription">
		  <div class="panel-heading">
		    <h3 class="panel-title">View Subscription</h3>
		  </div>
		  <div class="panel-body">
				  <div class="form-group col-sm-6 ">
			           <div class="input-group col-xs-12">
			               	<select class="form-control" id="selectpublications">
							</select>
						</div>
				</div>
				<div class="form-group col-sm-6 ">
		 				<button id="btnsubscribe" class="btn btn-primary col-sm-12"  type="button">Subscribe</button>	
			   	 </div>
			   	 
	   	 		<table class="table table-hover table-striped table-bordered" id="tbviewsubscription" >
			        <thead>
			            <tr>
			                <th>#</th>
			                <th class="hide">Journal id</th>
			                <th>Journal Name</th>
			                <th>Subscribed On</th>
			                <th></th>
			            </tr>
			        </thead>
			        <tbody>
	
			        </tbody>
		       </table>
		       
		       <button id="deletesubscriptions" class="btn btn-lg btn-primary btn-block"  type="button">UnSubscribe</button>	
		  </div>
		</div>
		

	</div>


    </div>
    

</body>
</html>