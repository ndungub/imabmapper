(function() { 

	root = window || this;
	//Set base URL
	root.baseUrl="http://" + location.host + "/imabmapper";
	
	  root.successBootbox = function(message) {
	    bootbox.dialog({
	      message: message,
	      title: 'Success!',
	      onEscape: function() {},
	      show: true,
	      backdrop: true,
	      closeButton: true,
	      animate: true,
	      buttons: {
	        'OK': {
	          className: 'btn-primary',
	          callback: function(result) {
	        	  $('.bootbox.modal').modal('hide');
	          }
	        }
	      }
	    });
	    $('.bootbox.modal').on('hidden.bs.modal', function (e) {
	        if($('.modal.in').length > 0){
	          $('body').addClass('modal-open');
	        }else{
	        	 $('body').removeClass('modal-open');
	        }
	     });  
	  	  
	 $('.bootbox.modal').on('hidden.bs.modal', function (e) {
	        if($('.modal.in').length > 0){
	          $('body').addClass('modal-open');
	        }else{
	         $('body').removeClass('modal-open');
	        }
	     });		
	  };

	  root.warningBootbox = function(message) {

	    bootbox.dialog({
	      message: message,
	      title: 'Warning!',
	      onEscape: function() {},
	      show: true,
	      backdrop: true,
	      closeButton: true,
	      animate: true,
	      buttons: {
	        'OK': {
	          className: 'btn-danger',
	          callback: function(result) {
	        	  $('.bootbox.modal').modal('hide');
	          }
	        }
	      }
	    });
	    
	    $('.bootbox.modal').on('hidden.bs.modal', function (e) {
	        if($('.modal.in').length > 0){
	          $('body').addClass('modal-open');
	        }else{
	         $('body').removeClass('modal-open');
	        }
	     });  
		 	  

	  };


	root.showProgress = function(progressMessage) {
			
		  if (!root.progress) {
			$('#progessMessage').text(progressMessage);
			root.progress = true;
		    return $("#progress-modal").modal("show");
		  }  
	};

	root.closeProgress = function() {
	  if (root.progress) {
		  root.progress = false;
	    return $("#progress-modal").modal("hide");
	  }
	  
	  $("#progress-modal").on('hidden.bs.modal', function (e) {
	      if($('.modal.in').length > 0){
	        $('body').addClass('modal-open');
	      }else{
	       $('body').removeClass('modal-open');
	      }
	   });  
	};

	root.homeView = function() {
		$.get("html/homeView.html", function(data){
			$('#container').html(data);
			
			loadHomeProjects();
			mapView.init();
		});

		$("body").on("change", ".project-check", function(event) {
			var id = $(this).attr("id");
			if($(this).prop("checked")) {
				$.ajax({
					type: "POST",
					contentType : "application/json",
					url: baseUrl + "/loadPlots/" + id,
					dataType: "json",
					success: function(result) {
						mapView.addProject(result);
					}
				});

				$.ajax({
					type: "POST",
					contentType : "application/json",
					url: baseUrl + "/loadPlotMap/" + id,
					dataType: "json",
					success: function(result) {
						mapView.addProjectMap(result);
					}
				});
			}
		});
	};
	
	loadHomeProjects = function() {
		$.ajax({
		    type: "GET",
		    contentType : "application/json",
		    url: baseUrl + "/allprojects",
		    dataType: "json", 
		    success: function(result) {
		    	var project='';
		    	$.each(result,function(i,data) {
		    		project +='<div class="checkbox"><label style="font-size: 1.5em"><input type="checkbox" value="" class="project-check" id="'+data.project_id+'"><span class="cr"><i class="cr-icon fa fa-check"></i></span>'+ data.project_name +'</label></div>';
		    		//project +='<div class="row"><div class="checkbox"><label><input type="checkbox" value="" class="project-check" id="'+data.project_id+'">'+ data.project_name +' </label></div></div>';
		    		 //project +='<div class="row"><div class="checkbox"><label><input type="checkbox" value="" class="project-check" id="'+data.project_id+'">'+ data.project_name +'</label></div></div>';
		    		 //project+='<div class="row"><div class="input-group" style="width: 100%"><span class="input-group-addon"><label><strong>'+ data.project_name +'</strong></label><input class="project-check" type="checkbox" id="'+ data.project_id +'"> </span></div></div>';

		    	});
		    	
		    	$('#projectlist').html(project);
		    }
		  });
	};

}).call(this);

$( document ).ready(function() {
	root.homeView();
});