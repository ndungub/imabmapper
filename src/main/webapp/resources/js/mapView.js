(function() { 

	root = window || this;

	root.mapView = {
		map : null,
		init : function() {
			map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: -1.3044564, lng: 36.7073082},
				scrollwheel: false,
				zoom: 8
			});
		},

		addProject : function(plots) {
			var bounds = new google.maps.LatLngBounds();
			var infowindow = new google.maps.InfoWindow();
			_.each(plots, function(plot) {
				if(plot.poi != null) {
					var coord = plot.poi.substring(plot.poi.indexOf("(") + 1, plot.poi.indexOf(")") - 1);
					var latlng = coord.split(" ");
					var plotLatLng = new google.maps.LatLng(latlng[0], latlng[1]);
					var marker = new google.maps.Marker({
						position: plotLatLng,
						title: plot.plotNo,
						icon: "https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_blue.png"
					});
					
					marker.addListener('click', function() {
						infowindow.setContent("<div>" + plot.plotNo + "</div>");
						infowindow.open(map, marker);
					});
					marker.setMap(map);
					bounds.extend(plotLatLng);
				}

			}, this);
			map.fitBounds(bounds);
		},

		addProjectMap : function(plotMap) {
			_.each(plotMap, function(line) {
				if(line.poi != null) {
					var coord = line.poi.substring(line.poi.indexOf("(") + 1, line.poi.indexOf(")") - 1);
					var points = coord.split(",");
					var lineCoordinates = [];
					_.each(points, function(point) {
						var x = point.split(" ");
						lineCoordinates.push({ lat : parseFloat(x[0]), lng: parseFloat(x[1])});
					});
					var plotLine = new google.maps.Polyline({
						path: lineCoordinates,
						geodesic: true,
						strokeColor: '#FF0000',
						strokeOpacity: 1.0,
						strokeWeight: 2
					});
					plotLine.setMap(map);
				}

			}, this);
		}
	}

	
}).call(this);