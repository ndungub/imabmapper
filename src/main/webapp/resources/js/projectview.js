(function() { 

	root = window || this;

	root.projectView = function(container) {
		$.get("html/projectView.html", function(data){
			$('#container').html(data);
			
			$('#addproject').click(function(e){
				var selectedIndex = $('#jqxgrid-projectview').jqxGrid('selectedrowindexes');
				$('#jqxgrid-projectview').jqxGrid('unselectrow', selectedIndex[0]);
				 
				$('#projectModalLabel').text(e.target.dataset.message);
				$('#projectModal').modal('show');
			});
			
			$('#projectModal').on('show.bs.modal', function (e ) {	
				$('#fm-project')[0].reset();
			});

		   $("#fm-project").validate({
			   ignore: [],
			   highlight : function(label) {
		            $(label).closest('.form-group').addClass('has-error');
		          },
		          unhighlight: function (element, errorClass) {
		              $(element).closest('.form-group').removeClass('has-error');
		          },
			   ignore: ""
		   });
		   
		   $("#fm-project").on('submit', function (evt) {		   
			   evt.preventDefault();
			   if (!$("#fm-project").valid()) {
				   root.warningBootbox("Fill in all the required fields!");
				   return;
			   }
			   var selectedIndex = $('#jqxgrid-projectview').jqxGrid('selectedrowindexes');
			   var rowData = $('#jqxgrid-projectview').jqxGrid('getrowdata', selectedIndex);
			   
			   
			   var projectObj = {};
			   $.each($(this).serializeArray(),function(i,elem) {
				   projectObj[elem.name] = elem.value;
			   });

			   projectObj['auth_username']=model.username;
			   
			   
			   var isFileSizeOk = true;
			   var uploadFile = null;
			   if (rowData == null) {
				   $('#fm-project input:file').each(function (key, filelist) {
					   var file = filelist.files[0];
					   uploadFile = file;
					   if (file == null) {
						   var img = $(this).closest('div').parent('div').find('img');
						   if ($(img).attr('src') == ""  || $(img).attr('src') == null) {
							   root.warningBootbox(filelist.dataset.filename + ' is required!');
							   isFileSizeOk = false;
							   return false;
						   }
					   }
					   projectObj['shape_file']  = 1;
				   });
				   
				   if (!isFileSizeOk) {
					   return;
				   }
			   }
			   

				var url,msg;
				if (rowData == null) {
					msg ='Add New Project';
					url = baseUrl + "/addproject/add";
				}else{
					msg ='Edit  Project';
					projectObj['project_id'] = rowData.project_id;
					url = baseUrl + "/addproject/edit";
				}
				
		    	bootbox.dialog({
		            message: msg,
		            title: "Project",
		            onEscape: function() {},
		            show: true,
		            backdrop: true,
		            closeButton: true,
		            animate: true,
		            buttons: {
		              'Cancel': {
		                className: 'btn-primary',
		                callback: function(result) {}
		              },
		              'OK': {
		                className: 'btn-primary',
		                callback: function(result) {
		                	root.showProgress("Updating Project..........");
		                	$.ajax({
		        			    type: "POST",
		        			    contentType : "application/json",
		        			    url: url,
		        			    data : JSON.stringify(projectObj),
		        			    dataType: "json", 
		        			    success: function(response) {
				   		        	root.closeProgress();
				   		        	if(response.status == "OK"){
				   		        		$("#fm-project")[0].reset();
				   		        		$('#projectModal').modal('hide');

										var formData = new FormData();
										formData.append("file", uploadFile);
										formData.append("projectId", response.project_id);

										$.ajax({
											url: baseUrl + '/uploadkml',
											data: formData,
											cache: false,
											processData: false,
											contentType: false,
											type: 'POST',
											success: function (data) {
												console.log("Uploaded kml");
											},
											error: function (data) {
												app.closeNotify(data.message, syncId, "error");
											}
										});

				   		        		loadProjects();
				   		        		
				   		        		root.successBootbox(response.message); 
				   		        	}else{
				   		        		root.warningBootbox(response.message);
				   		        	}
		        			    }
		        			  });
		                }
		              }
		            }
		          });
			   
		   });
		   
			loadProjects();
		});
	};
	
	loadProjects = function() {
		$('#jqxgrid-projectview').remove();
		$('#projectviewgrid').append('<div id="jqxgrid-projectview"></div>');
		
		var source =
		{
			datatype: "json",
			theme: 'energyblue',
			datafields: [
			    { name: 'rownum', type: 'number' },
				{ name: 'project_id', type: 'string'},
				{ name: 'project_name', type: 'string' },
			    { name: 'creation_date', type: 'date'},
			    { name: 'project_narration', type: 'string' },
			    { name: 'shape_file', type: 'string' },
			    { name: 'auth_username', type: 'string'}
			],
			url: baseUrl + "/allprojects",
			filter: function()
			{
				// update the grid and send a request to the server.
				$("#jqxgrid-projectview").jqxGrid('updatebounddata', 'filter');
			},
			cache: false
		};		
		var dataAdapter = new $.jqx.dataAdapter(source);

		// initialize jqxGrid
		$("#jqxgrid-projectview").jqxGrid(
		{		
			source: dataAdapter,
			width: '100%',
			//showfilterrow: true,
			pageable: true,
			pagesize: 20,
			autoheight: true,
			filterable: true,
			selectionmode: 'checkbox',
			showstatusbar: true,
	        statusbarheight: 25,
	        altrows: true,
	        showaggregates: true,
			enabletooltips: true,
			columnsresize: true,
			columns: [
	          {
	               text: '#', sortable: false, filterable: false, editable: false,
	               groupable: false, draggable: false, resizable: false,
	               datafield: '', columntype: 'number', width: 50,
	               cellsrenderer: function (row, column, value) {
	                   return "<div style='margin:4px;'>" + (value + 1) + "</div>";
	               }
	           },
	           { text: '', datafield: 'project_id' },
	           { text: 'Project Name', datafield: 'project_name', width: 300 },
	           { text: 'Project Narration', datafield: 'project_narration', width: 300 },
			   { text: 'Start Date', datafield: 'creation_date', width: 200,
	        	   columntype:'datetimeinput',filtertype:'date',cellsformat: 'yyyy-MM-dd',cellsalign:'center'   
			   },
			   { text: 'Created By', datafield: 'auth_username', width: 200 },
			   {text: '', datafield: 'Edit', columntype: 'button', width: 100,filterable: false, cellsrenderer: function () {
				   return "Edit";
				   },buttonclick: function (row) {
					   var id = $('#jqxgrid-projectview').jqxGrid('getrowid', row);;
					    $('#projectshape_file img').attr('src','');
						var data = $('#jqxgrid-projectview').jqxGrid('getrowdata', row); 
						
					    $('#projectModalLabel').text("Edit Project");
						$('#projectModal').modal('show');
						$('#projectModal').find('form')[0].reset();
						
					   $.each(data,function(key, value){
							if($('input[name=' + key + ']').is('input')){
								if (!$('input[name=' + key + ']').is('input:file')) {
									$('input[name=' + key + ']').val(value);
								}	
							}else if($('textarea[name=' + key + ']').is('textarea')){
								$('textarea[name=' + key + ']').val(value);
							}else if($('select[name=' + key + ']').is('select')){
								if ($('select[name=' + key + ']').hasClass('selectpicker')) {
									$('select[name=' + key + ']').selectpicker('val', value);
									$('select[name=' + key + ']').trigger('change');   
								}else{
									$('select[name=' + key + ']').val(value);
								}
							}else if($('#'+key).is('div')){
								if (moment(value).isValid()) {
									$('#'+key).val(new Date(value));
								}
								
							}else{
								//goes here
							}
					   });
				   }
			   },
			   {text: '', datafield: 'Remove', columntype: 'button', width: 100,filterable: false, cellsrenderer: function () {
				   return "Remove";
				   },buttonclick: function (row) {
					   var id = $('#jqxgrid-projectview').jqxGrid('getrowid', row);

					   
				   }
			   }
				
			],
			showtoolbar: true,
			toolbarheight: 30,
            rendertoolbar: function (toolbar) {
                var gridTitle = "<div style='width: 100%; height: 100%; text-align: center;'>Project View</div>";
                toolbar.append(gridTitle);
            }
		});
		$("#jqxgrid-projectview").on("bindingcomplete", function (event) { 
			$("#jqxgrid-projectview").jqxGrid('hidecolumn', 'project_id');
		});
		$('#jqxgrid-projectview').on('rowselect', function (event) {
            var index=$('#jqxgrid-projectview').jqxGrid('getselectedrowindexes');
            if( index.length >1 ) {
                $('#jqxgrid-projectview').jqxGrid('unselectrow', index[0]);
            }
             if( index.length >1 ) {
                 $('#jqxgrid-projectview').jqxGrid('unselectrow', index[0]);
             }
		});
		
	
	};
	
}).call(this);