package com.mapper.model;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;


import com.mapper.dto.Plot;
import com.mapper.dto.PlotMap;
import com.mapper.dto.Projects;
import com.mapper.dto.Clients;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.json.JSONObject;

/**
 * * Created by boniface on 26/08/2016.
 * Handles data manupulation
 */

public class DataAccess implements Serializable{
	protected static final Logger log = Logger.getLogger(DataAccess.class);

    private DataSource dataSource;
    
    //Initialize data source
	public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
	}
	
	//Get all projects return project list
	public List<Projects> getProjectList() {
	
		List<Projects> projList = new ArrayList<Projects>();
		
		Query q = null;
		String query="SELECT project_id,project_name,creation_date,project_narration,shape_file,auth_username FROM projectlist";

		
		try {
			
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Projects proj = new Projects();
				proj.setProject_id(rs.getInt(1));
				proj.setProject_name(rs.getString(2));
				proj.setCreation_date(rs.getString(3));
				proj.setproject_narration(rs.getString(4));
				proj.setshape_file(rs.getInt(5));
				proj.setAuth_username(rs.getString(6));
				
				projList.add(proj);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return projList;
	}
	
	//Get projectllist per user and return project list
	//Overrides all projects
	public List<Projects> getProjectList(String username) {
		List<Projects> projList = new ArrayList<Projects>();
		
		Query q = null;
		String query="SELECT project_id,project_name,creation_date,project_narration,shape_file,auth_username FROM projectlist WHERE auth_username='"+ username +"'";

		
		try {
			
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				Projects proj = new Projects();
				
				proj.setProject_id(rs.getInt(1));
				proj.setProject_name(rs.getString(2));
				proj.setCreation_date(rs.getString(3));
				proj.setproject_narration(rs.getString(4));
				proj.setshape_file(rs.getInt(5));
				proj.setAuth_username(rs.getString(6));
				
				projList.add(proj);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return projList;

	}

	//add new Project 
	public Map<String, Object> AddProject(Projects proj,String action) {
		HashMap<String, Object> listObj = new HashMap<String, Object>();
		
		String query =null;
		
		if (action.equalsIgnoreCase("add")) {
			 query="INSERT INTO projectlist "
					+ " (project_name,creation_date,project_narration,shape_file,auth_username) "
					+ "VALUES("
							+ "'"+ proj.getProject_name() +"',"
							+ "NOW(),"
							+ "'"+ proj.getproject_narration() +"',"
							+ ""+ proj.getshape_file() +","
							+ "'"+ proj.getAuth_username() +"');";
		}else{
			query="UPDATE  projectlist  "
					+ "project_name='"+ proj.getProject_name() +"',"
					+ "project_narration='"+ proj.getproject_narration() +"',"
					+ "shape_file='"+ proj.getshape_file() +"',"
					+ "auth_username='"+ proj.getAuth_username() +"'"
					+ "WHERE project_id='"+ proj.getProject_id() +"'";
		}

		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query,PreparedStatement.RETURN_GENERATED_KEYS);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int lastID=0;
            if (rs != null && rs.next()) {
            	lastID = rs.getInt(1);
            }
            
            listObj.put("status", "OK");
            listObj.put("message", "Project Saved");
            listObj.put("project_id", lastID);
			
		} catch (SQLException e) {
			log.error("Error adding project.", e);
            e.printStackTrace();
            listObj.put("status", "error");
            listObj.put("message", "Problem Saving Project");
            
		}
		
		return listObj;
		

	}
	
	//delete selected Project
	public boolean deleteProject(String username, int[] project_ids) {
		String project_id=Arrays.toString(project_ids);
		project_id=project_id.substring(1,project_id.length()-1);
		
		String query="DELETE FROM projectlist WHERE auth_username='"+ username +"' AND project_id IN (" + project_ids +")";

		
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
			
			
		} catch (SQLException e) {
			log.error("Error deleting project.", e);
            e.printStackTrace();
            return false;
		}
		

	}

	//Get all clients 
	public List<Clients> getClients() {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "A.auth_username"
				+ "FROM clients A";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setClientname(rs.getString(2));
				client.setClientidentity(rs.getString(3));
				client.setClientidentitytype(rs.getString(4));
				client.setClientphysicaladdress(rs.getString(5));
				client.setClientpinno(rs.getString(6));
				client.setClientpemail(rs.getString(7));
				client.setClientcountry(rs.getString(8));
				client.setClientcounty(rs.getString(9));
				client.setClientsubcounty(rs.getString(10));
				client.setAuth_username(rs.getString(11));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	
	//Get clients list per username
	public List<Clients> getUserClients(String username) {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "A.auth_username"
				+ "FROM clients A"
				+ "WHERE A.auth_username='"+ username +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setClientname(rs.getString(2));
				client.setClientidentity(rs.getString(3));
				client.setClientidentitytype(rs.getString(4));
				client.setClientphysicaladdress(rs.getString(5));
				client.setClientpinno(rs.getString(6));
				client.setClientpemail(rs.getString(7));
				client.setClientcountry(rs.getString(8));
				client.setClientcounty(rs.getString(9));
				client.setClientsubcounty(rs.getString(10));
				client.setAuth_username(rs.getString(11));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	//Add Client
	public Map<String, Object> AddClient(JSONObject clientObj) {
		
		/*int client_id,int project_id,
		String clientname, String clientidentity,String clientidentitytype,
		String clientphysicaladdress, String clientpinno,String clientpemail,
		String clienttelno, String clientnextofkin,String clientlrno,
		String clientplotno, String clientcountry,String clientcounty,
		String clientsubcounty, String clientacreage,String clientpp,
		String clientdepositpayable, String clientpercentagepaid,String clientbalanceofpp,
		String clientrecommendedbyname, String clientrecommendedbytel,String auth_username*/
		
		HashMap<String, Object> listObj = new HashMap<String, Object>();
		JSONObject client = clientObj.getJSONObject("client");
		
		
		String query="INSERT INTO clients ("
				+ "client_id,"
				+ "clientname"
				+ "clientidentity,"
				+ "clientidentitytype,"
				+ "clientphysicaladdress"
				+ "clientpinno,"
				+ "clientpemail,"
				+ "clienttelno"
				+ "clientcountry,"
				+ "clientcounty,"
				+ "clientsubcounty"
				+ "auth_username"
				+ ") VALUES("
						+ ""+ client.get("client_id")+","
						+ "'"+ client.get("clientname") +"'"
						+ "'"+ client.get("clientidentity") +"'"
						+ "'"+ client.get("clientidentitytype") +"'"
						+ "'"+ client.get("clientphysicaladdress") +"'"
						+ "'"+ client.get("clientpinno") +"'"
						+ "'"+ client.get("clientpemail") +"'"
						+ "'"+ client.get("clienttelno") +"'"
						+ "'"+ client.get("clientnextofkin") +"'"
						+ "'"+ client.get("clientcountry") +"'"
						+ "'"+ client.get("clientcounty") +"'"
						+ "'"+ client.get("clientsubcounty") +"'"
						+ "'"+ client.get("auth_username") +"'"
						+ ")";

		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            
            listObj.put("message", "success");
			
		} catch (SQLException e) {
			log.error("Error adding client.", e);
            e.printStackTrace();
            listObj.put("message", "error");
            listObj.put("error", e.getErrorCode());
		}
		
		return listObj;
		

	}

	//delete Client
	public boolean deleteClient(String username, int[] client_ids) {
		String client_id=Arrays.toString(client_ids);
		client_id=client_id.substring(1,client_id.length()-1);
		
		String query="DELETE FROM clients WHERE auth_username='"+ username +"' AND client_id IN (" + client_id +")";

		
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
			
			
		} catch (SQLException e) {
			log.error("Error deleting client.", e);
            e.printStackTrace();
            return false;
		}
		

	}

	//Get all client purchase order 
	public List<Clients> getAllClientPurchase() {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "B.project_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "C.clientnextofkin "
				+ "C.clientlrno,"
				+ "C.clientplotno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "C.clientacreage,"
				+ "C.clientpp,"
				+ "C.clientdepositpayable "
				+ "C.clientpercentagepaid,"
				+ "C.clientbalanceofpp,"
				+ "C.clientrecommendedbyname "
				+ "C.clientrecommendedbytel,"
				+ "C.auth_username,"
				+ "FROM clients A, projectlist B,purchaseorder C "
				+ "WHERE A.project_id=B.project_id "
				+ "AND A.client_id=C.client_id "
				+ "AND B.project_id=C.project_id ";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setProject_id(rs.getInt(2));
				client.setClientname(rs.getString(3));
				client.setClientidentity(rs.getString(4));
				client.setClientidentitytype(rs.getString(5));
				client.setClientphysicaladdress(rs.getString(6));
				client.setClientpinno(rs.getString(7));
				client.setClientpemail(rs.getString(8));
				client.setClienttelno(rs.getString(9));
				client.setClientnextofkin(rs.getString(10));
				client.setClientlrno(rs.getString(11));
				client.setClientplotno(rs.getString(12));
				client.setClientcountry(rs.getString(13));
				client.setClientcounty(rs.getString(14));
				client.setClientsubcounty(rs.getString(15));
				client.setClientacreage(rs.getString(16));
				client.setClientpp(rs.getString(17));
				client.setClientdepositpayable(rs.getString(18));
				client.setClientpercentagepaid(rs.getString(19));
				client.setClientbalanceofpp(rs.getString(20));
				client.setClientrecommendedbyname(rs.getString(21));
				client.setClientrecommendedbytel(rs.getString(22));
				client.setAuth_username(rs.getString(23));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	
	//Get all client purchase order by project
	public List<Clients> getClientPurchase(String project_id) {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "B.project_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "C.clientnextofkin "
				+ "C.clientlrno,"
				+ "C.clientplotno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "C.clientacreage,"
				+ "C.clientpp,"
				+ "C.clientdepositpayable "
				+ "C.clientpercentagepaid,"
				+ "C.clientbalanceofpp,"
				+ "C.clientrecommendedbyname "
				+ "C.clientrecommendedbytel,"
				+ "C.auth_username,"
				+ "FROM clients A, projectlist B,purchaseorder C "
				+ "WHERE A.project_id=B.project_id "
				+ "AND A.client_id=C.client_id "
				+ "AND B.project_id=C.project_id "
				+ "AND C.project_id='"+ project_id +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setProject_id(rs.getInt(2));
				client.setClientname(rs.getString(3));
				client.setClientidentity(rs.getString(4));
				client.setClientidentitytype(rs.getString(5));
				client.setClientphysicaladdress(rs.getString(6));
				client.setClientpinno(rs.getString(7));
				client.setClientpemail(rs.getString(8));
				client.setClienttelno(rs.getString(9));
				client.setClientnextofkin(rs.getString(10));
				client.setClientlrno(rs.getString(11));
				client.setClientplotno(rs.getString(12));
				client.setClientcountry(rs.getString(13));
				client.setClientcounty(rs.getString(14));
				client.setClientsubcounty(rs.getString(15));
				client.setClientacreage(rs.getString(16));
				client.setClientpp(rs.getString(17));
				client.setClientdepositpayable(rs.getString(18));
				client.setClientpercentagepaid(rs.getString(19));
				client.setClientbalanceofpp(rs.getString(20));
				client.setClientrecommendedbyname(rs.getString(21));
				client.setClientrecommendedbytel(rs.getString(22));
				client.setAuth_username(rs.getString(23));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	
	//Get client purchase order by user
	public List<Clients> getUserClientPurchase(String username) {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "B.project_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "C.clientnextofkin "
				+ "C.clientlrno,"
				+ "C.clientplotno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "C.clientacreage,"
				+ "C.clientpp,"
				+ "C.clientdepositpayable "
				+ "C.clientpercentagepaid,"
				+ "C.clientbalanceofpp,"
				+ "C.clientrecommendedbyname "
				+ "C.clientrecommendedbytel,"
				+ "C.auth_username,"
				+ "FROM clients A, projectlist B,purchaseorder C "
				+ "WHERE A.project_id=B.project_id "
				+ "AND A.client_id=C.client_id "
				+ "AND B.project_id=C.project_id "
				+ "AND A.auth_username='"+ username +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setProject_id(rs.getInt(2));
				client.setClientname(rs.getString(3));
				client.setClientidentity(rs.getString(4));
				client.setClientidentitytype(rs.getString(5));
				client.setClientphysicaladdress(rs.getString(6));
				client.setClientpinno(rs.getString(7));
				client.setClientpemail(rs.getString(8));
				client.setClienttelno(rs.getString(9));
				client.setClientnextofkin(rs.getString(10));
				client.setClientlrno(rs.getString(11));
				client.setClientplotno(rs.getString(12));
				client.setClientcountry(rs.getString(13));
				client.setClientcounty(rs.getString(14));
				client.setClientsubcounty(rs.getString(15));
				client.setClientacreage(rs.getString(16));
				client.setClientpp(rs.getString(17));
				client.setClientdepositpayable(rs.getString(18));
				client.setClientpercentagepaid(rs.getString(19));
				client.setClientbalanceofpp(rs.getString(20));
				client.setClientrecommendedbyname(rs.getString(21));
				client.setClientrecommendedbytel(rs.getString(22));
				client.setAuth_username(rs.getString(23));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}

	//Get client purchase order by project
	public List<Clients> getUserProjectClientPurchase(String username, int project_id) {
		
		List<Clients> clients = new ArrayList<Clients>();
		
		Query q = null;
		String query="SELECT "
				+ "A.client_id,"
				+ "B.project_id,"
				+ "A.clientidentity,"
				+ "A.clientidentitytype,"
				+ "A.clientphysicaladdress "
				+ "A.clientpinno,"
				+ "A.clientpemail,"
				+ "A.clienttelno,"
				+ "C.clientnextofkin "
				+ "C.clientlrno,"
				+ "C.clientplotno,"
				+ "A.clientcountry,"
				+ "A.clientcounty "
				+ "A.clientsubcounty,"
				+ "C.clientacreage,"
				+ "C.clientpp,"
				+ "C.clientdepositpayable "
				+ "C.clientpercentagepaid,"
				+ "C.clientbalanceofpp,"
				+ "C.clientrecommendedbyname "
				+ "C.clientrecommendedbytel,"
				+ "C.auth_username,"
				+ "FROM clients A, projectlist B,purchaseorder C "
				+ "WHERE A.project_id=B.project_id "
				+ "AND A.client_id=C.client_id "
				+ "AND B.project_id=C.project_id "
				+ "AND C.project_id='"+ project_id +"'"
				+ "AND A.auth_username='"+ username +"'";

		
		try {
			Connection connection = dataSource.getConnection();

            PreparedStatement ps = connection.prepareStatement(query);
            ResultSet rs = ps.executeQuery();
           
			while (rs.next()) {
				
				Clients client = new Clients();
				client.setClient_id(rs.getInt(1));
				client.setProject_id(rs.getInt(2));
				client.setClientname(rs.getString(3));
				client.setClientidentity(rs.getString(4));
				client.setClientidentitytype(rs.getString(5));
				client.setClientphysicaladdress(rs.getString(6));
				client.setClientpinno(rs.getString(7));
				client.setClientpemail(rs.getString(8));
				client.setClienttelno(rs.getString(9));
				client.setClientnextofkin(rs.getString(10));
				client.setClientlrno(rs.getString(11));
				client.setClientplotno(rs.getString(12));
				client.setClientcountry(rs.getString(13));
				client.setClientcounty(rs.getString(14));
				client.setClientsubcounty(rs.getString(15));
				client.setClientacreage(rs.getString(16));
				client.setClientpp(rs.getString(17));
				client.setClientdepositpayable(rs.getString(18));
				client.setClientpercentagepaid(rs.getString(19));
				client.setClientbalanceofpp(rs.getString(20));
				client.setClientrecommendedbyname(rs.getString(21));
				client.setClientrecommendedbytel(rs.getString(22));
				client.setAuth_username(rs.getString(23));

				
				clients.add(client);
			}
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return clients;
	}
	
	//Client Purchase Order
	public Map<String, Object> PurchasePlot(JSONObject clientObj) {

		HashMap<String, Object> listObj = new HashMap<String, Object>();
		JSONObject client = clientObj.getJSONObject("client");
		
		
		String query="INSERT INTO purchaseplot ("
				+ "client_id,"
				+ "project_id"
				+ "clientnextofkin,"
				+ "poi,"
				+ "clientlrno,"
				+ "clientplotno"
				+ "clientacreage,"
				+ "clientpp,"
				+ "clientdepositpayable"
				+ "clientpercentagepaid"
				+ "clientbalanceofpp"
				+ "clientrecommendedbyname"
				+ "clientrecommendedbytel"
				+ "purchasestatus"
				+ "auth_username"
				+ ") VALUES("
						+ ""+ client.get("client_id")+","
						+ "'"+ client.get("project_id") +"'"
						+ "'"+ client.get("poi") +"'"
						+ "'"+ client.get("clientnextofkin") +"'"
						+ "'"+ client.get("clientlrno") +"'"
						+ "'"+ client.get("clientplotno") +"'"
						+ "'"+ client.get("clientacreage") +"'"
						+ "'"+ client.get("clientpp") +"'"
						+ "'"+ client.get("clientdepositpayable") +"'"
						+ "'"+ client.get("clientpercentagepaid") +"'"
						+ "'"+ client.get("clientbalanceofpp") +"'"
						+ "'"+ client.get("clientrecommendedbyname") +"'"
						+ "'"+ client.get("clientrecommendedbytel") +"'"
						+ "'"+ client.get("purchasestatus") +"'"
						+ "'"+ client.get("auth_username") +"'"
						+ ")";

		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            
            listObj.put("message", "success");
			
		} catch (SQLException e) {
			log.error("Error adding client.", e);
            e.printStackTrace();
            listObj.put("message", "error");
            listObj.put("error", e.getErrorCode());
		}
		
		return listObj;
		

	}
	
	//delete Client
	public boolean deletePurchaseOrder(String username, int[] purchaseorder_ids) {
		String purchaseorder_id=Arrays.toString(purchaseorder_ids);
		purchaseorder_id=purchaseorder_id.substring(1,purchaseorder_id.length()-1);
		
		String query="DELETE FROM purchaseorder WHERE auth_username='"+ username +"' AND client_id IN (" + purchaseorder_id +")";

		
		try {
			Connection connection = dataSource.getConnection();
            PreparedStatement ps = connection.prepareStatement(query);
            ps.executeUpdate();
            return true;
			
			
		} catch (SQLException e) {
			log.error("Error deleting purchase order.", e);
            e.printStackTrace();
            return false;
		}
		

	}

	public boolean addPlot(Plot plot) {
		Connection connection = null;
		try {
			String sql = "INSERT INTO plot(lrno, plotno, acreage, project_id, poi) VALUES (?, ?, ?, ?, ST_GeomFromText('" + plot.getPoi() + "'))";
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, plot.getLrNo());
			ps.setString(2, plot.getPlotNo());
			ps.setDouble(3, plot.getAcreage());
			ps.setInt(4, plot.getProjectId());
			ps.execute();
			return true;
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {}
		}
		return false;
	}

	public List<Plot> loadPlots(Integer projectId) {
		Connection connection = null;
		List<Plot> plotList = new ArrayList<Plot>();
		try {
			String sql = "SELECT id, lrno, plotno, acreage, project_id, St_asText(poi) as poi FROM plot WHERE project_id = ?";
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, projectId);

			ResultSet r = ps.executeQuery();
			while(r.next()) {
				Plot p = new Plot();
				p.setProjectId(projectId);
				p.setId(r.getInt("id"));
				p.setLrNo(r.getString("lrno"));
				p.setPlotNo(r.getString("plotno"));
				p.setAcreage(r.getDouble("acreage"));
				p.setPoi(r.getString("poi"));
				plotList.add(p);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {}
		}
		return plotList;
	}

	public boolean addPlotMap(PlotMap plotMap) {
		Connection connection = null;
		try {
			String sql = "INSERT INTO plot_map(project_id, poi) VALUES (?, ST_GeomFromText('" + plotMap.getPoi() + "'))";
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, plotMap.getProjectId());
			ps.execute();
			return true;
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {}
		}
		return false;
	}

	public List<PlotMap> loadPlotMap(Integer projectId) {
		Connection connection = null;
		List<PlotMap> plotList = new ArrayList<PlotMap>();
		try {
			String sql = "SELECT id, project_id, St_asText(poi) as poi FROM plot_map WHERE project_id = ?";
			connection = dataSource.getConnection();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setInt(1, projectId);

			ResultSet r = ps.executeQuery();
			while(r.next()) {
				PlotMap p = new PlotMap();
				p.setProjectId(projectId);
				p.setId(r.getInt("id"));
				p.setPoi(r.getString("poi"));
				plotList.add(p);
			}
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		} finally {
			try {
				if(connection != null) {
					connection.close();
				}
			} catch (Exception e) {}
		}
		return plotList;
	}
	
	
}
