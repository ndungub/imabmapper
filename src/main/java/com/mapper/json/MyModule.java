package com.mapper.json;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.cglib.proxy.Mixin;

import java.lang.annotation.Target;


public class MyModule extends SimpleModule {

    private Class<?> minxin;
    private Class<?> target;
    public MyModule(Class<?> target, Class<?> mixin) {
        super("ModuleName", new Version(0,0,1,null));
        this.target = target;
        this.minxin = mixin;
    }
    @Override
    public void setupModule(SetupContext context)
    {
        context.setMixInAnnotations(target, minxin);
        // and other set up, if any
    }
}