package com.mapper.dto;

/**
 * Created by kenny on 14/09/2016.
 */
public class PlotMap {

    private int id;
    private int projectId;
    private String poi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getPoi() {
        return poi;
    }

    public void setPoi(String poi) {
        this.poi = poi;
    }
}
