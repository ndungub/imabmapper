package com.mapper.dto;

import java.io.Serializable;

/**
 * Created by boniface on 26/08/2016.
 * 
 */

//Publication object to map to corresponding JSON template
@SuppressWarnings("serial")
public class Clients implements Serializable{
	private int client_id;
	private int project_id;
	private String clientname;
	private String clientidentity;
	private String clientidentitytype;
	private String clientphysicaladdress;
	private String clientpinno;
	private String clientpemail;
	private String clienttelno;
	private String clientnextofkin;
	private String clientlrno;
	private String clientplotno;
	private String clientcountry;
	private String clientcounty;
	private String clientsubcounty;
	private String clientacreage;
	private String clientpp;
	private String clientdepositpayable;
	private String clientpercentagepaid;
	private String clientbalanceofpp;
	private String clientrecommendedbyname;
	private String clientrecommendedbytel;
	private String auth_username;
	private int[] client_ids;
	private int[] purchaseorder_ids;
	
	public int getClient_id() {
		return client_id;
	}
	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}
	
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getClientname() {
		return clientname;
	}
	public void setClientname(String clientname) {
		this.clientname = clientname;
	}
	public String getClientidentity() {
		return clientidentity;
	}
	public void setClientidentity(String clientidentity) {
		this.clientidentity = clientidentity;
	}
	public String getClientidentitytype() {
		return clientidentitytype;
	}
	public void setClientidentitytype(String clientidentitytype) {
		this.clientidentitytype = clientidentitytype;
	}
	public String getClientphysicaladdress() {
		return clientphysicaladdress;
	}
	public void setClientphysicaladdress(String clientphysicaladdress) {
		this.clientphysicaladdress = clientphysicaladdress;
	}
	public String getClientpinno() {
		return clientpinno;
	}
	public void setClientpinno(String clientpinno) {
		this.clientpinno = clientpinno;
	}
	public String getClientpemail() {
		return clientpemail;
	}
	public void setClientpemail(String clientpemail) {
		this.clientpemail = clientpemail;
	}
	public String getClienttelno() {
		return clienttelno;
	}
	public void setClienttelno(String clienttelno) {
		this.clienttelno = clienttelno;
	}
	public String getClientnextofkin() {
		return clientnextofkin;
	}
	public void setClientnextofkin(String clientnextofkin) {
		this.clientnextofkin = clientnextofkin;
	}
	public String getClientlrno() {
		return clientlrno;
	}
	public void setClientlrno(String clientlrno) {
		this.clientlrno = clientlrno;
	}
	public String getClientplotno() {
		return clientplotno;
	}
	public void setClientplotno(String clientplotno) {
		this.clientplotno = clientplotno;
	}
	public String getClientcountry() {
		return clientcountry;
	}
	public void setClientcountry(String clientcountry) {
		this.clientcountry = clientcountry;
	}
	public String getClientcounty() {
		return clientcounty;
	}
	public void setClientcounty(String clientcounty) {
		this.clientcounty = clientcounty;
	}
	public String getClientsubcounty() {
		return clientsubcounty;
	}
	public void setClientsubcounty(String clientsubcounty) {
		this.clientsubcounty = clientsubcounty;
	}
	public String getClientacreage() {
		return clientacreage;
	}
	public void setClientacreage(String clientacreage) {
		this.clientacreage = clientacreage;
	}
	public String getClientpp() {
		return clientpp;
	}
	public void setClientpp(String clientpp) {
		this.clientpp = clientpp;
	}
	public String getClientdepositpayable() {
		return clientdepositpayable;
	}
	public void setClientdepositpayable(String clientdepositpayable) {
		this.clientdepositpayable = clientdepositpayable;
	}
	public String getClientpercentagepaid() {
		return clientpercentagepaid;
	}
	public void setClientpercentagepaid(String clientpercentagepaid) {
		this.clientpercentagepaid = clientpercentagepaid;
	}
	public String getClientbalanceofpp() {
		return clientbalanceofpp;
	}
	public void setClientbalanceofpp(String clientbalanceofpp) {
		this.clientbalanceofpp = clientbalanceofpp;
	}
	public String getClientrecommendedbyname() {
		return clientrecommendedbyname;
	}
	public void setClientrecommendedbyname(String clientrecommendedbyname) {
		this.clientrecommendedbyname = clientrecommendedbyname;
	}
	public String getClientrecommendedbytel() {
		return clientrecommendedbytel;
	}
	public void setClientrecommendedbytel(String clientrecommendedbytel) {
		this.clientrecommendedbytel = clientrecommendedbytel;
	}
	public String getAuth_username() {
		return auth_username;
	}
	public void setAuth_username(String auth_username) {
		this.auth_username = auth_username;
	}
	public int[] getClient_ids() {
		return client_ids;
	}
	public void setClient_ids(int[] client_ids) {
		this.client_ids = client_ids;
	}
	public int[] getPurchaseorder_ids() {
		return purchaseorder_ids;
	}
	public void setPurchaseorder_ids(int[] purchaseorder_ids) {
		this.purchaseorder_ids = purchaseorder_ids;
	}
	



	

}
