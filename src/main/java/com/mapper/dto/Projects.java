package com.mapper.dto;

import java.io.Serializable;

/**
* Created by boniface on 26/08/2016.
 * 
 */

//Publication object to map to corresponding JSON template
@SuppressWarnings("serial")
public class Projects implements Serializable{
	private int project_id;
	private String project_name;
	private String auth_username;
	private String creation_date;
	private String project_narration;
	private int shape_file;
	
	private int[] project_ids;
	public int getProject_id() {
		return project_id;
	}
	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}
	public String getProject_name() {
		return project_name;
	}
	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}
	public String getAuth_username() {
		return auth_username;
	}
	public void setAuth_username(String auth_username) {
		this.auth_username = auth_username;
	}
	public String getCreation_date() {
		return creation_date;
	}
	public void setCreation_date(String creation_date) {
		this.creation_date = creation_date;
	}
	
	public String getproject_narration() {
		return project_narration;
	}
	public void setproject_narration(String project_narration) {
		this.project_narration = project_narration;
	}
	public int getshape_file() {
		return shape_file;
	}
	public void setshape_file(int shape_file) {
		this.shape_file = shape_file;
	}
	public int[] getProject_ids() {
		return project_ids;
	}
	public void setProject_ids(int[] projectid_ids) {
		this.project_ids = projectid_ids;
	} 
	


	
}
