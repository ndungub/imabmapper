package com.mapper.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mapper.dto.Plot;
import com.mapper.dto.PlotMap;
import de.micromata.opengis.kml.v_2_2_0.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.mapper.dto.Projects;
import com.mapper.dto.Clients;
import com.mapper.model.DataAccess;

/**
 * * Created by boniface on 26/08/2016.
 * 
 */


@Controller
public class MapperController {
	//Autowire Data access class instead of initializing everytime
	@Autowired
	private DataAccess dataAccess;

	  //Requesting mapping and redirection to appropriate views
	  //Root mapping to login 
	//Get all projects
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/allprojects", method = RequestMethod.GET)
	  public  List getAllProjects() {
		
		
		return dataAccess.getProjectList();
	     
	  }
	
	//Get  project by user
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/userprojects", method = RequestMethod.POST)
	  public  List getUserProjects(@RequestBody Projects pub) {
		
		return dataAccess.getProjectList(pub.getAuth_username());
	     
	  }

	//add new project
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/addproject/{action}", method = RequestMethod.POST)
	  public  Map<String, Object> AddProject(@RequestBody Projects proj,
			  @PathVariable String action) {
		
		
		return dataAccess.AddProject(proj, action);
	  }
	
	//delete selected projects
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/deleteproject", method = RequestMethod.POST)
	  public  List deleteProject(@RequestBody Projects proj) {
		
		List<Object> listObj =new ArrayList<Object>();
		
		if(dataAccess.deleteProject(proj.getAuth_username(),proj.getProject_ids())){
			listObj.add("success");
		}else{
			listObj.add("error");
		}
		
		return listObj;
	    
		
	  }

	//Get  all clients 
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/clients", method = RequestMethod.POST)
	  public  List getAllClients(@RequestBody Clients clients) {
		
		return dataAccess.getClients();
	     
	  }
	
	//Get  clients per user
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/userclients", method = RequestMethod.POST)
	  public  List getUserClients(@RequestBody Clients clients) {
		
		return dataAccess.getUserClients(clients.getAuth_username());
	     
	  }

	//add new client
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/addclient", method = RequestMethod.POST)
	  public  Map<String, Object> addClient(@RequestBody Clients clients) {
		
		JSONObject clientObj = new JSONObject(clients);
		return dataAccess.AddClient(clientObj);
	  }
	
	//delete selected clients
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/deleteclients", method = RequestMethod.POST)
	  public  List deleteClients(@RequestBody Clients clients) {
		
		List<Object> listObj =new ArrayList<Object>();
		
		if(dataAccess.deleteClient(clients.getAuth_username(),clients.getClient_ids())){
			listObj.add("success");
		}else{
			listObj.add("error");
		}
		
		return listObj;
	    
		
	  }

	//Get  purchase order
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/userclients/{user}/{project}", method = RequestMethod.POST)
	  public  List getPurchaseOrder(@RequestBody Clients clients,@RequestBody String user,
			  @RequestBody int project) {
		
		List<Clients> clientList = new ArrayList<Clients>();
		if (user == null && project == 0 ) {
			clientList= dataAccess.getAllClientPurchase();
		}else if(user != null && project == 0 ){
			clientList= dataAccess.getUserClients(clients.getAuth_username());
		}else {
			clientList= dataAccess.getUserProjectClientPurchase(user,project);
		}
		return clientList;
	     
	  }

	//add new client
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/placepurchaseorder/", method = RequestMethod.POST)
	  public  Map<String, Object> placePurchaseOrder(@RequestBody Clients clients) {
		
		JSONObject clientObj = new JSONObject(clients);
		return dataAccess.PurchasePlot(clientObj);
	  }
	
	//delete selected clients
	@SuppressWarnings("rawtypes")
	@ResponseBody  
	@RequestMapping(value = "/deletepurchaseorder", method = RequestMethod.POST)
	  public  List deletePurchaseOrder(@RequestBody Clients clients) {
		
		List<Object> listObj =new ArrayList<Object>();
		
		if(dataAccess.deleteProject(clients.getAuth_username(),clients.getPurchaseorder_ids())){
			listObj.add("success");
		}else{
			listObj.add("error");
		}
		
		return listObj;
	    
		
	  }
	
	
	//Upload images to upload folder
    @RequestMapping(value="/uploadfile", method=RequestMethod.POST )
    public @ResponseBody String saveFile(@RequestParam("file") MultipartFile file, @RequestParam("customfilename") String customfilename, Principal principal ){
    	
    	Map<String, Object> listObj = new HashMap<String, Object>();
    	String fileName = null;
    	if (!file.isEmpty()) {
            try {
            	//update database
            	//listObj= dataAccess.AddProject(principal.getName(), customfilename);
            	
            	if(listObj.get("message").equals("success")){
            		//check if folder upload exists first
            		Path path = Paths.get(System.getProperty("catalina.base")+"/webapps/uploads");
            		if (!Files.exists(path)) {
            			try {
            				path = Paths.get(System.getProperty("catalina.base")+"/webapps/uploads");
            				
            				Files.createDirectory(path);

                        } catch (IOException e) {
                            //fail to create directory
                            e.printStackTrace();
                        }
            		}
            		
	            	fileName = listObj.get("project_id").toString() +"_" + principal.getName() +".pdf";
	                byte[] bytes = file.getBytes();
	                
	                BufferedOutputStream buffStream = 
	                        new BufferedOutputStream(new FileOutputStream(new File(System.getProperty("catalina.base") +"/webapps/uploads/" + fileName)));
	                buffStream.write(bytes);
	                buffStream.close();
            	}
             
                
                
            } catch (Exception e) {
            	//On error return error object
                e.printStackTrace();
                listObj.put("message", "error");
                listObj.put("error", "couldn't upload file");
                
            }
        } else {
            listObj.put("message", "error");
            listObj.put("error", "couldn't upload file");
        }
    	
    	if(listObj.get("message").equals("success")){
    		return "redirect:imab";
    	}else{
    		return (String) listObj.get("error");
    	}
		
    }

    /**
     *  Handles upload of KML File to the server.
     *  Should save the coordinates.
     * @param file
     * @return
     */
	@RequestMapping(value="/uploadkml", method=RequestMethod.POST )
	public @ResponseBody String uploadKML(@RequestParam("file") MultipartFile file, @RequestParam("projectId") Integer projectId) {
		try {
			if(!file.isEmpty()) {
				File finalFile = getFileFromMultipart(file);
				final Kml kml = Kml.unmarshal(finalFile);
				Feature f = kml.getFeature();
				if(f instanceof Document) {
					Document doc = (Document)f;
                    List<Feature> docFeatures = doc.getFeature();
                    for(Feature docFeature : docFeatures) {
                        if(docFeature instanceof Folder) {
                            Folder folder = (Folder) docFeature;
                            List<Feature> features = folder.getFeature();
                            for (Feature feature : features) {
                                Placemark placemark = (Placemark) feature;
                                String name = placemark.getName();
								Plot plot = new Plot();
								plot.setPlotNo(name);
								plot.setProjectId(projectId);
								if(placemark.getGeometry() instanceof Point) {
									Point point = (Point) placemark.getGeometry();
									List<Coordinate> coordinates = point.getCoordinates();

									for (Coordinate coordinate : coordinates) {
										plot.setPoi("POINT(" + coordinate.getLatitude() + " " + coordinate.getLongitude() + ")");
									}
									dataAccess.addPlot(plot);
								} else if(placemark.getGeometry() instanceof LineString) {
									LineString lineString = (LineString)placemark.getGeometry();
									List<Coordinate> coordinates = lineString.getCoordinates();

									StringBuilder line = new StringBuilder();
									line.append("LINESTRING(");
									boolean first = true;
									for (Coordinate coordinate : coordinates) {
										if(!first) {
											line.append(",");
										}
										line.append(coordinate.getLatitude());
										line.append(" ");
										line.append(coordinate.getLongitude());
										first = false;
										//plot.setPoi("POINT(" + coordinate.getLatitude() + " " + coordinate.getLongitude() + ")");
									}
									line.append(")");
									PlotMap plotMap = new PlotMap();
									plotMap.setProjectId(projectId);
									plotMap.setPoi(line.toString());
									dataAccess.addPlotMap(plotMap);
								}

                            }
                        }
                    }

				}

			}
            return "uploaded";
		} catch (Exception e) {
            e.printStackTrace();
		}
        return "null";

	}

	public File getFileFromMultipart(MultipartFile file) throws IllegalStateException, IOException {
		File finalFile = new File(file.getOriginalFilename());
		file.transferTo(finalFile);
		return finalFile;
	}

	@ResponseBody
	@RequestMapping(value = "/loadPlots/{projectId}", method = RequestMethod.POST)
	public List<Plot> loadPlots(@PathVariable("projectId") Integer projectId) {
		return dataAccess.loadPlots(projectId);
	}

	@ResponseBody
	@RequestMapping(value = "/loadPlotMap/{projectId}", method = RequestMethod.POST)
	public List<PlotMap> loadPlotMap(@PathVariable("projectId") Integer projectId) {
		return dataAccess.loadPlotMap(projectId);
	}
}
