REQUIREMENTS:
1. Make sure maven2 in installed in your compiling machine.
   https://maven.apache.org/docs/2.2.1/release-notes.html
   
2. The database of choice is MySQL, the database script is included under root project folder. 
   Run it normally like any other MySQL database script.
   
3. Spring was the technology of choice, it's largely supported and by used og hibernate on can manage
   database transactions optimally even in a case where the dateset get large.


5. To compile the project, under root project folder run:
   1.cd imabmapper
   2. mvn clean
   (For initial test)http://marketplace.eclipse.org/marketplace-client-intro?mpc_install=1789445
   3. mvn package -Pfe
   (Subsquent runs)
   4. mvn package
   To access the web portal copy the respective wire file under target folder to your tomcat webapp folder and run in browser of choice:
   (NB: use your respective port number)
   5. http://localhost:8080/imabmapper/
