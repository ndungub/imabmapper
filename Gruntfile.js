module.exports = function(grunt) {
	var prefix = grunt.option('dest') || 'js';
	

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: [
              'src/main/webapp/resources/js/mapper.js',
              'src/main/webapp/resources/js/projectView.js',
              'src/main/webapp/resources/js/clientView.js',
              'src/main/webapp/resources/js/mapView.js',
              'src/main/webapp/resources/js/tableView.js',
              'src/main/webapp/resources/js/clients.js'
              ],
        dest: 'src/main/webapp/resources/js/imabmapper.js'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
    	  files: {
              'src/main/webapp/resources/js/imabmapper.min.js': ['<%= concat.dist.dest %>']
            }
      }
    },

    jshint: {
        all: ['Gruntfile.js']
    },
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');



  grunt.registerTask('default', [ 'concat', 'uglify']);

};